namespace IcaiAspDotNet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Registrations",
                c => new
                    {
                        FirstName = c.String(nullable: false, maxLength: 128),
                        LastName = c.String(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        EmailId = c.String(nullable: false),
                        Password = c.String(nullable: false, maxLength: 150),
                        UserType = c.String(),
                        UserName = c.Int(nullable: false),
                        FullName = c.String(),
                    })
                .PrimaryKey(t => t.FirstName);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Registrations");
        }
    }
}
