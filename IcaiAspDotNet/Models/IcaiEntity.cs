﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using IcaiAspDotNet.Models;
namespace IcaiAspDotNet.Models
{
    public class IcaiEntity : DbContext
    {
        public IcaiEntity() : base("IcaicmeportalEntities")
        {

        }

        public DbSet<Registration> Registrations { get; set; }
    }
}