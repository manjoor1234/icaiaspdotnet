﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IcaiAspDotNet.Models
{
    public class Login
    {
        [Key]
        [Required(ErrorMessage = "The E-mail address is required")]
        [Display(Name = "Email Id")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [EmailAddress(ErrorMessage = "Invalid E-mail Address")]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "Password is a required field.")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(150, MinimumLength = 6)]
        public string Password { get; set; }
    }
}