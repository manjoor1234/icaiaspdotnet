﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using IcaiAspDotNet.Models;
using System.Web.Security;
using System.Net.Mail;
using System.Net;

namespace IcaiAspDotNet.Controllers
{
    public class RegistrationController : Controller
    {
        private object console;

        // GET: Registration       
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Home()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

       
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login data)
        {
            string message = "";
            if (ModelState.IsValid)
            {
                try
                {
                    using (IcaicmeportalEntities db = new IcaicmeportalEntities())
                    {
                        var obj = db.Registrations.Where(a => a.EmailId.Equals(data.EmailId)).FirstOrDefault();
//                            db.Registrations.Where(a => a.EmailId.Equals(data.EmailId)).FirstOrDefault();
                        if (obj != null)
                        {
                            FormsAuthentication.SetAuthCookie(obj.FullName, false);
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            message = "Invalid credential provided";
                        }
                    }
                }
                catch
                {
                    ViewBag.message = "Invalid Login credentails";                   
                }
            }
            ViewBag.Message = message;
            ModelState.AddModelError("", "Invalid username and password");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude = "UserName,FullName")] Registration data)
        {
            string message = "";
            if (ModelState.IsValid)
            {
                try
                {   
                    Random rand = new Random();
                    data.UserName = rand.Next(100000, 999999); // minimum = 0, maximum = 999
                    data.FullName = data.FirstName + ' ' + data.LastName;
                    data.UserType = "Member";
                    data.Password = encryptpass(data.Password);
                   
                    using (IcaicmeportalEntities db = new IcaicmeportalEntities())
                    {
                        db.Registrations.Add(data);
                        db.SaveChanges();
                       //if(i>0)
                       // {
                            Sendmailtouser(data.EmailId, data.UserName,data.Password);
                            message = "Registration has been done,And Account activation link" + "has been sent your eamil id:" + data.EmailId;
                            return RedirectToAction("Login");
                       // }
                       // else
                       // {
                           // message = "Registration has been Faild";
                            //ViewBag.Message = message;
                           // return View();
                       // }
                    }
                }
                catch
                {
                    message = "";
                }
            }
            ViewBag.Message = message;
            return View();
        }

        [NonAction]
        public string encryptpass(string password)
        {
            string pass = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            pass = Convert.ToBase64String(encode);
            return pass;
        }

        [NonAction]
        public void Sendmailtouser(string emailid,int username,string password)
        {
            //var scheme = Request.Url.Scheme;
            //var host = Request.Url.Host;
            //var port = Request.Url.Port;

           // Guid activationCode = Guid.NewGuid();
            var fromMail = new MailAddress("manjooralam474@gmail.com", "welcome Manjoor Alam");
            var toMail = new MailAddress(emailid);
            var frontEmailPassowrd = "Manjoor123@";
            string subject = "Your account is successfull created";
            string body = "Hello" + username + " < br/><br/>We are excited to tell you that your account is" +
            " successfully created. Please click on the below link to verify your account" +
            " <br/><br/>";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromMail.Address, frontEmailPassowrd)
            };
            using (var message = new MailMessage(fromMail, toMail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);

        }
    }
}