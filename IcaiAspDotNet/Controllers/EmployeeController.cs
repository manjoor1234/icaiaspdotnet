﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IcaiAspDotNet.Models;
namespace IcaiAspDotNet.Controllers
{
    public class EmployeeController : ApiController
    {
        IcaicmeportalEntities db = new IcaicmeportalEntities();
        [HttpGet]
        public IHttpActionResult GetEmployee()
        {
            List<Registration> register = new List<Registration>();
            register = (from i in db.Registrations select i).ToList();
            if(register.Count==0)
            {
                return NotFound();
            }
            else
            {
                return Ok(register);
            }

        }
    }
}
