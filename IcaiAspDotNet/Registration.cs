//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IcaiAspDotNet
{
    using System;
    using System.Collections.Generic;
    
    public partial class Registration
    {
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public System.DateTime DateofBirth { get; set; }
        public int UserName { get; set; }
        public string UserType { get; set; }
        public System.Guid IsActive { get; set; }
        public string FullName { get; set; }
    }
}
