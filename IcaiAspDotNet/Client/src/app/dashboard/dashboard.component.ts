import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private httpService: HttpClient) { }
  public employeeList : any [];
  ngOnInit() {

    this.httpService.get('http://localhost:49273/Employee/GetEmployee').subscribe(
      data => {
        console.log('hello')
      }
    )
  }

}
